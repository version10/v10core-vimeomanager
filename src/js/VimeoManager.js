// let Vimeo = require('vimeo').Vimeo;
// import AES from './AES';

// VimeoManager v1.1.0
if (typeof window.V10Core === "undefined") window.V10Core = {};

V10Core.VimeoManager = function (options) {
    this.options = {
        clientId: null,
        clientSecret: null,
        accessToken: null,
        aesKey: null
    };

    for (let o in options) {
        this.options[o] = options[o];
    }

    if (this.options.clientId == null || this.options.clientSecret == null || this.options.accessToken == null || this.options.aesKey == null) {
        return;
    }

    this.init();
}

V10Core.VimeoManager.prototype.init = function () {
    let clientId = AES.decrypt(this.options.clientId, this.options.aesKey);
    let clientSecret = AES.decrypt(this.options.clientSecret, this.options.aesKey);
    let accessToken = AES.decrypt(this.options.accessToken, this.options.aesKey);

    this.client = new Vimeo(clientId, clientSecret, accessToken);
    this.uploadBlockData = {
        files: null,
        name: null,
        description: null,
        uploadStartCallback: null,
        progressCallback: null,
        completeCallback: null,
        errorCallback: null
    };
}

V10Core.VimeoManager.prototype.addUploadEvents = function (button, input, nameInput, descriptionInput, onUploadBlockData, onUploadStartCallback, progressCallback, onCompleteCallback, onErrorCallback) {
    var _this = this;

    button.off('click').on('click', function () {
        if (nameInput.val() != '') {
            nameInput.removeClass('error');
            input.trigger('click');
        } else {
            nameInput.addClass('error');
        }
    });

    input.off('change').on('change', function (e) {
        var files = e.dataTransfer ? e.dataTransfer.files : $(e.target).get(0).files;

        _this.uploadBlockData.files = files[0];
        _this.uploadBlockData.name = nameInput.val();
        _this.uploadBlockData.description = descriptionInput.val();
        _this.uploadBlockData.uploadStartCallback = onUploadStartCallback;
        _this.uploadBlockData.progressCallback = progressCallback;
        _this.uploadBlockData.completeCallback = onCompleteCallback;
        _this.uploadBlockData.errorCallback = onErrorCallback;

        onUploadBlockData(_this.uploadBlockData);
    });
}

V10Core.VimeoManager.prototype.startVideoUpload = function(){
    this.uploadBlockData.uploadStartCallback();
    this.uploadVideo(this.uploadBlockData.files, this.uploadBlockData.name, this.uploadBlockData.description, this.uploadBlockData.progressCallback, this.uploadBlockData.completeCallback, this.uploadBlockData.errorCallback);
}

V10Core.VimeoManager.prototype.addDeleteEvents = function (button, uri, completeCallback, errorCallback) {
    var _this = this;

    button.off('click');
    button.on('click', function () {
        _this.deleteVideo(uri, completeCallback, errorCallback);
    });
}

V10Core.VimeoManager.prototype.uploadVideo = function (path, name, description, progressCallback, onCompleteCallback, onErrorCallback) {
    progressCallback(0);
    this.client.upload(
        path,
        {
            'name': name,
            'description': description
        },
        function (uri) {
            //console.log('Your video URI is: ' + uri);
            onCompleteCallback(uri);
        },
        function (bytes_uploaded, bytes_total) {
            var percentage = (bytes_uploaded / bytes_total * 100).toFixed(2)
            //console.log(bytes_uploaded, bytes_total, percentage + '%')
            progressCallback(percentage);
        },
        function (error) {
            //console.log('Failed because: ' + error)
            onErrorCallback('Une erreur est survenue.');
        }
    )
}

V10Core.VimeoManager.prototype.deleteVideo = function (uri, completeCallback, errorCallback) {
    var _this = this;

    this.getVideoId(uri, function (videoId) {
        _this.client.request({
            method: 'DELETE',
            path: '/videos/' + videoId
        }, function (error, body, status_code, headers) {
            if (error) {
                errorCallback(error);
            } else {
                completeCallback(error);
            }
        });
    });
}

V10Core.VimeoManager.prototype.getVideoId = function (uri, callback) {
    this.client.request(uri + '?fields=link', function (error, body, status_code, headers) {
        $.ajax({
            url: 'https://vimeo.com/api/oembed.json?url=' + body.link,
            async: false,
            success: function (response) {
                if (response.video_id) {
                    callback(response.video_id);
                }
            }
        });
    })
}

V10Core.VimeoManager.prototype.checkTranscode = function (uri, transcodeCallback) {
    this.client.request(uri + '?fields=transcode.status', function (error, body, status_code, headers) {
        transcodeCallback(body.transcode.status);
    })
}

V10Core.VimeoManager.prototype.getLink = function (uri, linkCallback) {
    this.client.request(uri + '?fields=link', function (error, body, status_code, headers) {
        if (error) {
            // console.log('There was an error making the request.')
            // console.log('Server reported: ' + error)
            return;
        }

        linkCallback(body.link);
    })
}

V10Core.VimeoManager.prototype.initVideoPlayersByUri = function (selector, attribute, callback) {
    var _this = this;

    $.each(selector, function (i, e) {
        let vimeoUri = $(e).attr(attribute);
        _this.getLink(vimeoUri, function (link) {
            callback(e, link);
        });
    });

}

V10Core.VimeoManager.prototype.version = "1.1.0";