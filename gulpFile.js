//Class Name
var className = "VimeoManager";

//Import
var gulp = require('gulp');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var pump = require('pump');

//Script
function scripts(cb){
    pump([
        gulp.src('./src/js/**/*.js'),
        concat(className+'.min.js'),
        uglify({output:{comments : true}}),
        gulp.dest('./dist/')
    ],cb)
};

exports.scripts = scripts;